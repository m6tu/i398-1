package gol;


import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

@SuppressWarnings("unused")
public class GameOfLifeTest {

    // The good place to start is to be able to mark cells as alive.

    // Then it is possible to count alive neighbors.

    // Then try to calculate next frame
    // (at first something very simple)
    // Do not implement all the roles at once

    // If you need some helper method eg. Frame.toString() then write test to them as well.

    // It is strongly advised to use TDD.
    // It usually shows from the result whether TDD was used or not.

}